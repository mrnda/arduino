

const int one[7] ={LOW,LOW,LOW,HIGH,LOW,LOW,LOW};
const int two[7] ={HIGH,LOW,LOW,LOW,HIGH, LOW,LOW};
const int three[7] ={HIGH,LOW,LOW,HIGH,HIGH,LOW,LOW};
const int four[7] = {HIGH, LOW, HIGH,LOW, HIGH, LOW,HIGH};
const int five[7] = {HIGH, LOW,HIGH,HIGH,HIGH,LOW,HIGH};
const int six [7] = {HIGH, HIGH,HIGH,LOW,HIGH,HIGH,HIGH};

#define BUTTON_PIN 12

int previous;
int buttonStillPressed = 0;

void setup()
{
  int i = 1;
  for(i = 1; i<9;i++){
    pinMode(i, OUTPUT);
  }
  Serial.begin(19200);
}



int getRandomNumber(){
   int i = random(1,7);
   if(i == previous){
     return getRandomNumber();
   }
   previous = i;
   return i;
  pinMode(BUTTON_PIN, INPUT);
}
void writeNumber(const int numberArray[]){
  int i = 1;
  for(i = 2; i < 9; i++){
     digitalWrite(i, numberArray[i-2]);
  }

}

void loop()
{
   long number = getRandomNumber();  
   int buttonPressed = digitalRead(BUTTON_PIN);
   if(buttonPressed==LOW){
     buttonStillPressed = 0;
     switch(number){
       case 1:
         writeNumber(one);
         break;
       case 2:
         writeNumber(two);
         break;
       case 3:
          writeNumber(three);
         break;
        case 4:
          writeNumber(four);
          break;
        case 5:
           writeNumber(five);
           break;
        case 6:
         writeNumber(six);
         break;
  
  
     }
      delay(500);
   } else if(buttonPressed == HIGH && !buttonStillPressed == 1){
     int repeat = 1;
     buttonStillPressed=1;
     for(repeat = 1; repeat<5;repeat++){
        number = getRandomNumber();
        switch(number){
         case 1:
           writeNumber(one);
           break;
         case 2:
           writeNumber(two);
           break;
         case 3:
            writeNumber(three);
           break;
          case 4:
            writeNumber(four);
            break;
          case 5:
             writeNumber(five);
             break;
          case 6:
           writeNumber(six);
           break;
       }
       delay(repeat*500);
     }
   }

}

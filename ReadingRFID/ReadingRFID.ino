#include<SoftwareSerial.h> // Přidání knihovny

int pin_tx = 6; // TX pin RFID
int pin_rx = 7; // RX pin RFID

int pin_led_red = 2; // pin LEDky


SoftwareSerial RFID(pin_tx, pin_rx); // Nastavení sériové linky

int cip[12] = {48, 51, 49, 48, 51, 48, 49, 50, 55, 57,13, 10}; //Můj čip

int position = 0; // Proměnná pro uložení pozice v poli
void setup()
{
  RFID.begin(9600); //Otevření kanálu pro komunikaci s modulem
  Serial.begin(9600); //Otevření kanálu pro komunikaci s PC
  pinMode(pin_led_red, OUTPUT); // Nastavení pinu LEDky na výtsupní
}

void loop()
{
  if(RFID.available() > 0) // Kontrola jestli modul posílá nějaká data ke čtení
  {

    int i = RFID.read(); // Načtení dat do proměnné

    if(cip[position++]==i) // Kontrola jestli je číslo shodné s číslem námi zadaného čipu.
    {
      if(position == 12){ // Pokud už jsme na konci --> všechny čísla byla shodná 
        position = 0; // nastavíme pozici na 0
        digitalWrite(pin_led_red, HIGH); // a rozsvítíme LEDku
      }

    }
    else // Pokud se čísla neshodují --> čip není ten náš
    {
      position = 0; // nastavíme pozici na 0
      digitalWrite(pin_led_red, LOW); // zhasneme LEDku
    }

    Serial.println(i); // Vypíšeme poslední načtené číslo čipu
  }  
}
